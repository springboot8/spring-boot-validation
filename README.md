# Spring-boot-validation

Exemplo de API Springboot com banco de dados H2 e tratamento de exceções.

## Tecnologias utilizadas

* Java, versão: 8
* Maven
  * Spring Boot, versão: 2.1.13.RELEASE
  * Spring Data Jpa
  * Spring boot devtools
  * H2 Database

## Urls

Todas as urls responderão no formato **JSON**.

## Usuário
#### Cadastra usuário

```
POST - http://localhost:8091/usuario/cadastro
```

Request
```
{
    "id": number,
    "nmUsuario": "string",
    "dsEmail": "string"
}
```

Response 
```
O retorno será o id do usuario cadastrado.
```

#### Recupera lista de usuários

```
GET - http://localhost:8091/usuario/lista
```

Request
```
Nenhum parâmetro passado na requisição.
```

Response 
```
[
    {
        "id": number,
        "nmUsuario": "string",
        "dsEmail": "string"
    }
]
```

## BANCO DE DADOS H2 - BROWSER URL CONSOLE: 
```
http://localhost:8091/h2-console
```
