package com.spring.boot.validation.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.spring.boot.validation.entidades.Usuario;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Long>{

}
