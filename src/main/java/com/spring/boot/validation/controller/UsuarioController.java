package com.spring.boot.validation.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.boot.validation.entidades.Usuario;
import com.spring.boot.validation.service.UsuarioService;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@PostMapping("/cadastro")
	public ResponseEntity<?> cadastrar(@RequestBody @Valid Usuario usuario) {
		usuarioService.save(usuario);
		return ResponseEntity.ok(usuario.getId());
	}
	
	@GetMapping("/lista")
	public ResponseEntity<?> lista() {
		return ResponseEntity.ok(usuarioService.lista());
	}

}
