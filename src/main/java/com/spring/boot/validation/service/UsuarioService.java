package com.spring.boot.validation.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.boot.validation.entidades.Usuario;
import com.spring.boot.validation.repository.UsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	public void save(Usuario usuario) {
		usuarioRepository.save(usuario);
	}
	
	public List<Usuario> lista() {
		List<Usuario> lista = new ArrayList<>();
		usuarioRepository.findAll().forEach(usuario -> lista.add(usuario));
		return lista;
	}
	
}
